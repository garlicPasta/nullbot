
const DEF_LIGHT = 0; // change 0 to 1 to get a weaker AI with lower CPU usage

include("multiplay/skirmish/nullbot-header-x.js.inc");

include("multiplay/skirmish/nullbot-generic-x.js.inc");

include("multiplay/skirmish/nullbot-main-x.js.inc");
